#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#if defined(_WIN32)
#include <C:/Users/michael/Documents/glut-3.7/include/GL/glut.h>
#include <Windows.h>
#pragma comment(lib, "C:/Users/michael/Documents/glut-3.7/lib/glut32.lib")
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>

#include "bmpread.h"


#pragma comment(lib, "C:/Users/michael/Documents/glut-3.7/lib/glut32.lib")

GLfloat xRotated, yRotated, zRotated;
GLdouble base = 1;
GLdouble height = 1.5;
GLint slices = 50;
GLint stacks = 50;


#define PI 3.1415926535898
#define Cos(th) cos(PI/180*(th))
#define Sin(th) sin(PI/180*(th))
#define DEF_D 5

/*  Globals */
double dim = 9.0;
const char *windowName = "Lab 2";
int windowWidth = 600;
int windowHeight = 550;

int toggleAxes = 0;
int toggleValues = 1;
int toggleMode = 0;   /* projection mode */
int th = 20;
int ph = 20;   /* elevation of view angle */
int fov = 55; /* field of view for perspective */
int asp = 1;  /* aspect ratio */
double LookAtX = 0.0;
double LookAtY = 0.0;
double LookAtZ = 0.0;

float sphereZ = 1.0; // Morphing

int objId = 0;

double light1PositionY = 0.0;
double light0PositionX = 0.0;

typedef struct
{
	int X;
	int Y;
	int Z;
	double U;
	double V;
}VERTICES;

typedef struct
{
	float x;
	float y;
	float z;
}vector3d;

typedef struct
{
	float x;
	float y;
	float z;
}stVec;

vector3d normal;
const int space = 10;
const int VertexCount = (90 / space) * (360 / space) * 4;
VERTICES VERTEX[VertexCount];
GLuint texture = 0;


/* For a sphere objects*/
void calcNormal(float p1_x, float p1_y, float p1_z, float p2_x, float p2_y, float p2_z, float p3_x, float p3_y, float p3_z)
{
	// Calculate vectors
	float var1_x = p2_x - p1_x;
	float var1_y = p2_y - p1_y;
	float var1_z = p2_z - p1_z;

	float var2_x = p3_x - p1_x;
	float var2_y = p3_y - p1_y;
	float var2_z = p3_z - p1_z;

	// Get cross product of vectors
	normal.x = (var1_y * var2_z) - (var2_y * var1_z);
	normal.y = (var1_z * var2_x) - (var2_z * var1_x);
	normal.z = (var1_x * var2_y) - (var2_x * var1_y);

	// Normalise final vector
	float vLen = sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));

	normal.x = normal.x / vLen;
	normal.y = normal.y / vLen;
	normal.z = normal.z / vLen;

}



void drawCone(GLuint texture) {
	float radius = 1;
	float r = 1;
	float h = 3;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	float t, s;
	float i = 0.05;

	for (s = 0.0; s<1.0; s += i)
	{
		for (t = 0.0; t <= 1.0; t += i)
		{
			float r = ((h - t) / h)*radius;
			glBegin(GL_POLYGON);
			glTexCoord2f(s, t);
			glVertex3f(r*cos(2 * PI*s), t, r*sin(2 * PI*s));
			glTexCoord2f(s + i, t);
			glVertex3f(r*cos(2 * PI*(s + i)), t, r*sin(2 * PI*(s + i)));
			glTexCoord2f(s + i, t + i);
			glVertex3f(r*cos(2 * PI*(s + i)), (t + i), r*sin(2 * PI*(s + i)));
			glTexCoord2f(s, t + i);
			glVertex3f(r*cos(2 * PI*s), (t + i), r*sin(2 * PI*s));
			glEnd();
		}
	}

	glDisable(GL_TEXTURE_2D);
}

void drawCube(GLuint texture, int size = 1) {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//glRotatef (90, 1, 0, 0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
	// top
	glColor3f(1.0f, 0.0f, 0.0f);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-0.5f * size, 0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(0.5f * size, 0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 1.0 * size);
	glVertex3f(0.5f * size, 0.5f * size, -0.5f * size);
	glTexCoord2f(0.0, 1.0 * size);
	glVertex3f(-0.5f * size, 0.5f * size, -0.5f * size);

	

	glEnd();

	glBegin(GL_QUADS);
	// front
	glColor3f(0.0f, 1.0f, 0.0f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(0.5f * size, -0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(0.5f * size, 0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 1.0 * size);
	glVertex3f(-0.5f * size, 0.5f * size, 0.5f * size);
	glTexCoord2f(0.0, 1.0 * size);
	glVertex3f(-0.5f * size, -0.5f * size, 0.5f * size);
	
	

	glEnd();

	glBegin(GL_QUADS);
	// right
	glColor3f(0.0f, 0.0f, 1.0f);
	glNormal3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(0.5f * size, 0.5f * size, -0.5f * size);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(0.5f * size, 0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 1.0 * size);
	glVertex3f(0.5f * size, -0.5f * size, 0.5f * size);
	glTexCoord2f(0.0, 1.0 * size);
	glVertex3f(0.5f * size, -0.5f * size, -0.5f * size);
	

	glEnd();

	glBegin(GL_QUADS);
	// left
	glColor3f(0.0f, 0.0f, 0.5f);
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(-0.5f * size, -0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(-0.5f * size, 0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 1.0 * size);
	glVertex3f(-0.5f * size, 0.5f * size, -0.5f * size);
	glTexCoord2f(0.0, 1.0 * size);
	glVertex3f(-0.5f * size, -0.5f * size, -0.5f * size);
	glTexCoord2f(0.0, 0.0);
	

	glEnd();

	glBegin(GL_QUADS);
	// bottom
	glColor3f(0.5f, 0.0f, 0.0f);
	glNormal3f(0.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(0.5f * size, -0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(-0.5f * size, -0.5f * size, 0.5f * size);
	glTexCoord2f(1.0 * size, 1.0 * size);
	glVertex3f(-0.5f * size, -0.5f * size, -0.5f * size);
	glTexCoord2f(0.0, 1.0 * size);
	glVertex3f(0.5f * size, -0.5f * size, -0.5f * size);
	
	
	glEnd();

	glBegin(GL_QUADS);
	// back
	glColor3f(0.0f, 0.5f, 0.0f);
	glNormal3f(0.0f, 0.0f, -1.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(0.5f * size, 0.5f * size, -0.5f * size);
	glTexCoord2f(1.0 * size, 0.0);
	glVertex3f(0.5f * size, -0.5f * size, -0.5f * size);
	glTexCoord2f(1.0 * size, 1.0 * size);
	glVertex3f(-0.5f * size, -0.5f * size, -0.5f * size);
	glTexCoord2f(0.0, 1.0 * size);
	glVertex3f(-0.5f * size, 0.5f * size, -0.5f * size);

	glEnd();
	glDisable(GL_TEXTURE_2D);
}


void drawCylinder() {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, texture);
	glEnable(GL_TEXTURE_2D);
	glColor4f(1.0, 1.0, 1.0, 0.5);
	glScalef(-1.0f, 1.0f, 1.0f);
	/* top triangle */
	double i, resolution = 0.01;
	double height = 6;
	double radius = 2;

	glPushMatrix();
	glTranslatef(0, -0.5, 0);

	glBegin(GL_TRIANGLE_FAN);
	glTexCoord2f(0.5, 0.5);
	glVertex3f(0, height, 0);  /* center */
	for (i = 2 * PI; i >= 0; i -= resolution)

	{
		glTexCoord2f(0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f);
		glVertex3f(0, height, 0);
	}
	/* close the loop back to 0 degrees */
	glTexCoord2f(0.5, 0.5);
	glVertex3f(0, height, 0);
	glEnd();

	/* bottom triangle: note: for is in reverse order */
	glBegin(GL_TRIANGLE_FAN);
	glTexCoord2f(0.5, 0.5);
	glVertex3f(0, 0, 0);  /* center */
	for (i = 0; i <= 2 * PI; i += resolution)
	{
		glTexCoord2f(0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f);
		glVertex3f(0, 0, 0);
	}
	glEnd();

	/* middle tube */
	glBegin(GL_QUAD_STRIP);
	for (i = 0; i <= 2 * PI; i += resolution)
	{
		const float tc = (i / (float)(2 * PI));
		glTexCoord2f(tc, 0.0);
		glVertex3f(0, 0, 0);
		glTexCoord2f(tc, 1.0);
		glVertex3f(radius * cos(i), height, radius * sin(i));
	}
	/* close the loop back to zero degrees */
	glTexCoord2f(0.0, 0.0);
	glVertex3f(0, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(radius, height, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
}

void drawSphere(float r, int n)
{
	float len = r;
	int inc = n;
	int z, x;

	float pi = PI;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//glRotatef (90, 1, 0, 0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glEnable(GL_TEXTURE_2D);

	for (z = 0; z<90; z += inc)
	{
		float dist1 = len * sin((pi*z) / 180);
		float dist2 = len * sin((pi*(z + inc)) / 180);

		float dist1a = sqrt(len*len - dist1 * dist1);
		float dist2a = sqrt(len*len - dist2 * dist2);

#if(1)       	 
		for (x = 0; x<360; x += inc)
		{
			float x1 = dist1a * cos((2 * pi*x) / 360);
			float y1 = dist1a * sin(2 * pi*x / 360);

			float x2 = dist1a * cos(2 * pi*(x + inc) / 360);
			float y2 = dist1a * sin(2 * pi*(x + inc) / 360);

			float x3 = dist2a * cos(2 * pi*x / 360);
			float y3 = dist2a * sin(2 * pi*x / 360);

			float x4 = dist2a * cos(2 * pi*(x + inc) / 360);
			float y4 = dist2a * sin(2 * pi*(x + inc) / 360);

			vector3d p1, p2, p3;

			p1.x = x1; p1.y = y1; p1.z = dist1;
			p2.x = x2; p2.y = y2; p2.z = dist1;
			p3.x = x4; p3.y = y4; p3.z = dist2;
			calcNormal(p2.x, p2.y, p2.z, p1.x, p1.y, p1.z, p3.x, p3.y, p3.z);

			glNormal3f(normal.x, normal.y, normal.z);
			glBegin(GL_QUADS);
			glTexCoord2f(0.0, 0.0);  glVertex3f(x1, y1, dist1);
			glTexCoord2f(0.0, 1.0);  glVertex3f(x2, y2, dist1);
			glTexCoord2f(1.0, 1.0);  glVertex3f(x4, y4, dist2);
			glTexCoord2f(1.0, 0.0);  glVertex3f(x3, y3, dist2);
			glEnd();

			p1.x = x1; p1.y = y1; p1.z = -dist1;
			p2.x = x2; p2.y = y2; p2.z = -dist1;
			p3.x = x4; p3.y = y4; p3.z = -dist2;
			calcNormal(p2.x, p2.y, p2.z, p1.x, p1.y, p1.z, p3.x, p3.y, p3.z);

			glNormal3f(normal.x, normal.y, normal.z);
			glBegin(GL_QUADS);
			glTexCoord2f(0.0, 0.0); glVertex3f(x1, y1, -dist1);
			glTexCoord2f(0.0, 1.0); glVertex3f(x2, y2, -dist1);
			glTexCoord2f(1.0, 1.0); glVertex3f(x4, y4, -dist2);
			glTexCoord2f(1.0, 0.0); glVertex3f(x3, y3, -dist2);
			glEnd();

		}
#endif       
	}
	glDisable(GL_TEXTURE_2D);
}

GLuint loadTexture(const char * bitmap_file)
{
	bmpread_t bitmap;

	if (!bmpread(bitmap_file, 0, &bitmap))
	{
		fprintf(stderr, "%s: error loading bitmap file\n", bitmap_file);
		exit(1);
	}

	
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, bitmap.width, bitmap.height, GL_RGB, GL_UNSIGNED_BYTE, bitmap.data);

	bmpread_free(&bitmap);

	return texture;
}


void project() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(fov, asp, dim / 4, 4 * dim);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void setEye(double x, double y, double z) {

	double Ex = -2 * dim*Sin(th)*Cos(ph);
	double Ey = +2 * dim        *Sin(ph);
	double Ez = +2 * dim*Cos(th)*Cos(ph);
	gluLookAt(Ex + x, Ey + y, Ez + z, 0, 0, 0, 0, Cos(ph), 0);

}

void drawAxes() {
	if (toggleAxes) {
		double len = 2.0;
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);
		glVertex3d(0, 0, 0);
		glVertex3d(len, 0, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, len, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, 0, len);
		glEnd();
		glRasterPos3d(len, 0, 0);
		glRasterPos3d(0, len, 0);
		glRasterPos3d(0, 0, len);
	}
}

void vertex(double th2, double ph2) {
	double x = Sin(th2)*Cos(ph2);
	double y = Cos(th2)*Cos(ph2);
	double z = sphereZ * Sin(ph2);
	glVertex3d(x, y, z);
}

void drawShape() {
	int th2, ph2, i, j, k;

	int len = 5;

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(len, 0, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, len, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, len);
	glEnd();

	drawCylinder();

	glEnable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	glTranslatef(3, 0, 0);
	glColor4f(1, 0, 0, 0.8);
	drawCube(texture, 3);
	//glutSolidCube(3);

	glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);

	/* ------------------- Lights --------------------- */
	float diffuseWhite[4] = { 1, 1, 1, 1 };
	float specularWhite[4] = { 1, 1, 1, 1 };
	GLfloat light0_position[] = { light0PositionX, 0.0, 2.0, 0.0 };
	

	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularWhite);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseWhite);
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);


	float diffuseColor[4] = { 1, 1, 0.2, 1 };
	float specularColor[4] = { 1, 1, 1, 1 };
	float ambientColor[4] = {1, 1, 1, 0.5};
	GLfloat light1_position[] = { 0.0, light1PositionY, 2.0, 0.0 };

	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specularColor);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseColor);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.0);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.2);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.4);


	glEnable(GL_LIGHT2);
	glLightfv(GL_LIGHT2, GL_AMBIENT, ambientColor);
	glLightfv(GL_LIGHT2, GL_SPECULAR, specularWhite);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, diffuseWhite);
	glLightfv(GL_LIGHT2, GL_POSITION, light0_position);


	glEnable(GL_LIGHTING);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseColor);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100.0);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularColor);
	/* ------------------- Lights --------------------- */

	


	glTranslatef(4, 0, 0);
	glColor4f(0, 1, 0, 1);
	//glutSolidSphere(2, 16, 16);
	//createSphere(30, 0, 0, 0);
	//displaySphere(5, texture);
	drawSphere(2, 8);
	
}


void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glLoadIdentity();

	

	setEye(LookAtX, LookAtY, LookAtZ);

	drawAxes();
	
	drawShape();

	glFlush();
	glutSwapBuffers();
}


void reshape(int width, int height) {
	asp = (height>0) ? (double)width / height : 1;
	glViewport(0, 0, width, height);
	project();
}


void windowKey(unsigned char key, int x, int y) {

	if (key == 32) {
		if (objId == 4) objId = 0;
		else objId++;
	}

	else if (key == '-' && key>1) fov--;
	else if (key == '+' && key<179) fov++;
	else if (key == 'a') dim += 0.1;
	else if (key == 'd' && dim>1) dim -= 0.1;
	else if (key == 'x') sphereZ += 0.1;
	else if (key == 'z') sphereZ -= 0.1;
	else if (key == 'o') light1PositionY += 0.1;
	else if (key == 'p') light1PositionY -= 0.1;
	else if (key == 'l') light0PositionX += 0.1;
	else if (key == ';') light0PositionX -= 0.1;

	project();
	glutPostRedisplay();
}


void windowSpecial(int key, int x, int y) {
	if (key == GLUT_KEY_RIGHT) th += 5;
	else if (key == GLUT_KEY_LEFT) th -= 5;
	else if (key == GLUT_KEY_UP) ph += 5;
	else if (key == GLUT_KEY_DOWN) ph -= 5;


	th %= 360;
	ph %= 360;

	project();
	glutPostRedisplay();
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow(windowName);

	loadTexture("example.bmp");

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(windowKey);
	glutSpecialFunc(windowSpecial);

	glutMainLoop();
	return 0;
}